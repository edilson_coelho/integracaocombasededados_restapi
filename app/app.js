import express from 'express';
import bodyParser from 'body-parser';
import expressValidator from 'express-validator'
import configuracoes from './settings';

// importar rotas
import produtoRoutes from "./src/routes/produto.route";

// importar middlewares
import { errorHandler } from "./src/middlewares/error-handler.middleware";

// importar validadores customizados
import { customValidators } from "./src/middlewares/validators/custom.validator";
import DemoContext from "./src/models/demo-context";

// Class Application
class App {

    constructor() {
        this.app = express(); // atribuição da instância da aplicação do tipo :Express

        this.configuracoes(); // registar configurações na aplicação
        this.registarMiddlewares(); // registar middlewares
        this.registarRotas(); // registar nossas rotas
    }

    configuracoes() {
        this.app.set('port', configuracoes.PORT);
        this.app.set('env', 'DEVELOPMENT');
    }

    registarMiddlewares() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser({ urlencoded: true }));
        this.app.use(expressValidator(customValidators)); // Deve ser registado após o bodyParser

        this.app.use(errorHandler);
    }

    registarRotas() {
        this.app.use('/api', produtoRoutes);
    }
}

// exportar aplicação
export default new App();
