import { CategoriaRepository } from '../repositories/categoria.repository';

// categoria.service
export class CategoriaService {

    constructor() {
        this.categoriaRepository = new CategoriaRepository();
    }

    inserir(categoria) {
        this.categoriaRepository.inserir(categoria);
    }

    obterCategoria(id) {
        return this.categoriaRepository.obterCategoria(id);
    }

    remover(id) {
        this.categoriaRepository.remover(id);
    }

    actualizar(categoria) {
        this.categoriaRepository.actualizar(categoria);
    }

    listarCategorias() {
        return this.categoriaRepository.listarCategorias();
    }
}
