import DemoContext from '../models/demo-context'

// produto.repository
export class ProdutoRepository {

    constructor() { }

    /**
     * Inserir novo produto
     * @param {*} produto 
     */
    inserir(produto) {
        DemoContext.Produtos.create(produto);
    }

    /**
     * Pesquisar produto, passando como filtro o Id
     * @param {*} id 
     */
    obterProduto(id) {
        return DemoContext.Produtos.find({ where: { id: id } });
    }

    /**
     * Remover produto
     * @param {*} id 
     */
    remover(id) {
        DemoContext.Produtos.destroy({ where: { id: id } });
    }

    /**
     * Actualizar informações do produto
     * @param {*} produto 
     */
    actualizar(produto) {
        DemoContext.Produtos.update(produto, { where: { id: produto.id } });
    }

    /**
     * Pesquisar todos os produtos
     */
    listarProdutos() {
        return DemoContext.Produtos.findAll();
    }
}
