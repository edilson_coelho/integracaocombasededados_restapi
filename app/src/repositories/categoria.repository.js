import DemoContext from '../models/demo-context'

// categoria.repository
export class CategoriaRepository {

    constructor() { }

    /**
     * Inserir nova categoria
     * @param {*} categoria 
     */
    inserir(categoria) {
        DemoContext.Categorias.create(categoria);
    }

    /**
     * Pesquisar categoria, passando como filtro o Id
     * @param {*} id 
     */
    obterCategoria(id) {
        return DemoContext.Categorias.find({ where: { id: id } });
    }

    /**
     * Remover categoria
     * @param {*} id 
     */
    remover(id) {
        DemoContext.Categorias.destroy({ where: { id: id } });
    }

    /**
     * Actualizar informações da categoria
     * @param {*} categoria 
     */
    actualizar(categoria) {
        DemoContext.Categorias.update(categoria, { where: { id: categoria.id } });
    }

    /**
     * Pesquisar todas as categorias
     */
    listarCategorias() {
        return DemoContext.Categorias.findAll();
    }
}
