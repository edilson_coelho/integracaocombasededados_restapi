import { DataTypes } from "sequelize";

// categoria.model
export class Categoria {

    constructor() {
        // atributos da Class Categoria
        this.descricao = { type: DataTypes.STRING(100), allowNull: false };
        this.estado = { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true };
    }
}

export const CategoriaModel = new Categoria(); // exportar o modelo Categoria
