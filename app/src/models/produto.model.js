import { DataTypes } from "sequelize";

// produto.model
export class Produto {

    constructor() {
        // atributos da class Produto
        this.descricao = { type: DataTypes.STRING(200), allowNull: false };
        this.quantidade = { type: DataTypes.INTEGER, allowNull: false };
        this.preco = { type: DataTypes.DECIMAL(10, 2), allowNull: false };
    }
}

export const ProdutoModel = new Produto(); // exportar o modelo Produto