import Sequelize from 'sequelize';
import settings from '../../settings'; // importar configurações

// importar modelos
import { ProdutoModel } from './produto.model';
import { CategoriaModel } from './categoria.model';

// importar relacionamentos
import categoriaRelationship from './relationships/categoria.relationship';
import produtoRelationship from './relationships/produto.relationship';

class DemoContext {

  constructor() {
    this.sequelizeInstance = new Sequelize(settings.URI, settings.SequelizeOptions); // cria a instância do sequelize, aqui estamos a estabelecer a ligação com a base de dados "demorestapi"
    this.checarConexao(); // verifica o estado da ligação com a base de dados 'demorestapi' e apresenta o resultado na consola

    this.registarModelos();
    this.syncDb();
  }

  checarConexao() {
    this.sequelizeInstance.authenticate().then((success) => {
      console.log('Ligação com a base de dados estabelecida com sucesso.');
    }).catch((error) => {
      console.log('Não foi possível estabelecer a ligação com a base de dados.');
    });
  }

  // Definindo nossos modelos que representam as tabelas na base de dados
  registarModelos() {
    this.Produtos = this.sequelizeInstance.define('produto', ProdutoModel);
    this.Categorias = this.sequelizeInstance.define('categoria', CategoriaModel);

    categoriaRelationship(this);
  }

  // Sincronização dos modelos com a base de dados
  syncDb() {
    this.sequelizeInstance.sync({ force: false, logging: true });
  }
}

export default new DemoContext(); // exportar DemoContext

