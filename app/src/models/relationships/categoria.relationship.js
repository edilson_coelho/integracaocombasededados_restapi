export default (demoContext) => {

    // categoria -> produto
    demoContext.Categorias.hasMany(demoContext.Produtos, {
        foreignKey: 'categoriaId'
    });
    
}
