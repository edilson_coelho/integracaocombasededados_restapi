export const errorHandler = (error, req, res, next) => {
    res.status(500);
    res.send('Ocorreu uma falha ao tentar satisfazer o pedido.');
}
