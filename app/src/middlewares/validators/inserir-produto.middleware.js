import { validationResult } from 'express-validator/check';

export default (req, res, next) => {
    // regras de validação
    
    req.checkBody('Id')
    .notEmpty().withMessage('Campo de preenchimento obrigatório.')
    .isInt().withMessage('Id inválido, aceita apenas valores numéricos.')
    .greaterThan(0).withMessage('O Id deve ser superior a zero.');

    req.checkBody('Descricao')
        .notEmpty().withMessage('Campo de preenchimento obrigatório.')
        .isLength({ min: 15 }).withMessage('De ter no mínimo (15) caracteres.');

    req.checkBody('Quantidade')
        .notEmpty().withMessage('Campo de preenchimento obrigatório.')
        .isInt().withMessage('Quantidade inválida, aceita apenas valores numéricos.')
        .greaterThan(0).withMessage('A quantidade deve ser superior a zero.');

    req.checkBody('Preco')
        .notEmpty().withMessage('Campo de preenchimento obrigatório.')        
        .isDecimal().withMessage('Preço inválido, aceita apenas valores decimais.')
        .greaterThan(0).withMessage('O Preço deve ser superior a zero.');;

    const errors = req.validationErrors();
    if (errors) {
        res.status(400); // http status code  400 -> Bad Request
        res.send({ errors });
    } else {
        next();
    }
}

