import { ProdutoController } from '../controllers/produto.controller';
import express from 'express';

// validators
import consultarProdutoValidator from "../middlewares/validators/consultar-produto.middleware";
import inserirProdutoValidator from "../middlewares/validators/inserir-produto.middleware";

const produtoController = new ProdutoController();

// produto.route
class ProdutoRoute {

    constructor() {
        this.routes = express.Router(); // atribuição da instância das rotas do tipo :Router

        // registar as rotas do produto
        this.registarRotas();
    }

    registarRotas() {
        this.routes.post('/produtos', produtoController.postRegistarNovoProduto);
        this.routes.put('/produtos', produtoController.putActualizarProduto);
        this.routes.get('/produtos', produtoController.getObterLitagemDeProdutos);
        this.routes.get('/produtos/:id', consultarProdutoValidator, produtoController.getObterProdutoPorId);
        this.routes.delete('/produtos/:id', produtoController.deleteRemoverProduto);
    }
}

// exportar rotas do produto
export default new ProdutoRoute().routes;
