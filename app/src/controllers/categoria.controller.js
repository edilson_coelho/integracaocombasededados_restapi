import { CategoriaService } from "../services/categoria.service";

const categoriaService = new CategoriaService();

// categoria.controller
export class CategoriaController {

    constructor() { }

    postRegistarNovaCategoria(req, res) {
        const payload = req.body; // extrair dados no corpo (body) da requisição

        try {
            categoriaService.inserir(payload); // função responsável pela inserção dos dados
            res.json({ messagem: 'Categoria inserida com sucesso.' });
        } catch (error) {
            res.json({ messagem: 'erro' });
        }
    }

    getObterCategoriaPorId(req, res) {
        const id = parseInt(req.params['id']); // obter id nos parâmetros da url

        categoriaService.obterCategoria(id).then(categoria => {
            res.json({ messagem: 'ok', categoria: categoria });
        }).catch(error => {
            res.json({ messagem: 'erro' });
        });
    }

    deleteRemoverCategoria(req, res) {
        const id = parseInt(req.params['id']); // obter id nos parâmetros da url

        try {
            categoriaService.remover(id); // função responsável pela eliminação de uma categoria
            res.json({ messagem: `Categoria (${id}) removida com sucesso.` });
        } catch (error) {
            res.json({ messagem: 'erro' });
        }
    }

    putActualizarCategoria(req, res) {
        const payload = req.body; // extrair dados no corpo da requisição

        try {
            categoriaService.actualizar(payload);
            res.json({ messagem: `Categoria (${payload.id}) actualizada com sucesso.` });
        } catch (error) {
            res.json({ messagem: 'erro' });
        }
    }

    getObterLitagemDeCategorias(req, res) {
        // listar todas as categorias existentes na tabela 'categoria' na base de dados 'demorestapi'
        categoriaService.listarCategorias().then(categorias => {
            res.json({ messagem: 'ok', categorias: categorias });
        }).catch(error => {
            res.json({ messagem: 'erro' });
        });
    }
}
