import { ProdutoService } from "../services/produto.service";

const produtoService = new ProdutoService();

// produto.controller
export class ProdutoController {

    constructor() { }

    postRegistarNovoProduto(req, res) {
        const payload = req.body; // extrair dados no corpo (body) da requisição

        try {
            produtoService.inserir(payload); // função responsável pela inserção dos dados
            res.json({ messagem: 'Produto inserido com sucesso.' });
        } catch (error) {
            res.json({ messagem: 'erro' });
        }
    }

    getObterProdutoPorId(req, res) {
        const id = parseInt(req.params['id']); // obter id nos parâmetros da url

        produtoService.obterProduto(id).then(produto => {
            res.json({ messagem: 'ok', produto: produto });
        }).catch(error => {
            res.json({ messagem: 'erro' });
        });
    }

    deleteRemoverProduto(req, res) {
        const id = parseInt(req.params['id']); // obter id nos parâmetros da url

        try {
            produtoService.remover(id); // função responsável pela eliminação de um produto
            res.json({ messagem: `Produto (${id}) removido com sucesso.` });
        } catch (error) {
            res.json({ messagem: 'erro' });
        }
    }

    putActualizarProduto(req, res) {
        const payload = req.body; // extrair dados no corpo da requisição

        try {
            produtoService.actualizar(payload);
            res.json({ messagem: `Produto (${payload.id}) actualizado com sucesso.` });
        } catch (error) {
            res.json({ messagem: 'erro' });
        }
    }

    getObterLitagemDeProdutos(req, res) {
        // listar todos os produtos existentes na tabela 'produto' na base de dados 'demorestapi'
        produtoService.listarProdutos().then(produtos => {
            res.json({ messagem: 'ok', produtos: produtos });
        }).catch(error => {
            res.json({ messagem: 'erro' });
        });
    }
}
