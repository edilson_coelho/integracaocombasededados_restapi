import http from 'http'; // Pacote core do Node.js
import Application from "./app"; // importar a aplicação (app.js)

const server = http.createServer(Application.app); // criação do nosso servidor

// Iniciar o servidor na porta 3000
server.listen(Application.app.get('port'), () => {
    console.log(`Servidor iniciado na porta ${Application.app.get('port')}.`);
});
